function obj = set_terminal_constraint(obj, h_x, g, is_soft)
%SET_CONSTRAINTS Set constraint matrices Hx, Hu and g
%
%    Input(s):
%    (1) obj     - GCMPC class instance
%    (2) h_x     - Constraint state matrix
%    (4) g       - Constraint affine term
%    (5) is_soft - Should constraints be soft? (optional, defaults to false)
%
%    Author(s):
%    (1) Carlos M. Massera
%
%    Note(s):
%    (1) Resulting constraint is of the form Hx x_k + Hu u_k + g <= 0

    if ~obj.is_system_set
        error('System matrices not set, define them before the constraint')
    end
    
    if obj.is_terminal_constraint_set
         warning('Terminal constraint definition is replaced, make sure your code is correct')
    end
    
    if nargin <= 3
        is_soft = false;
    end
    
    % Get constraint size
    n_c = size(h_x, 1);
    
    % Check matricies consistency
    if size(h_x, 2) ~= obj.n_x
        error('Hx matrix size does not match A matrix');
    end
    
    if size(g, 1) ~= n_c
        error('g vector size does not match Hc matrix');
    end
    
    if size(g, 2) ~= 1
        error('g is not a vector');
    end
    
    % Set instance variables
    obj.h_t_x = h_x;
    obj.g_t = g;
    obj.n_t_c = n_c;
    obj.is_terminal_constraint_soft = is_soft;
    obj.is_terminal_constraint_set = true;
end

