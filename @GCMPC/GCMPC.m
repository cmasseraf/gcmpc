classdef GCMPC < handle
% GCMPC This class defines the GCMPC problem and generates the controller for it.
    
    % TODO: Make all this private
    properties(SetAccess=private, GetAccess=public, Hidden)
        % System matrices 
        %     x_k+1 = A x_k + Bu u_k + Br r_k
        % Note(s):
        %     (1) Br is optional
        a   = [];
        b_u = [];
        b_r = [];
        
        % Disturbance matrices 
        %     x_k+1 = A x_k + Bu u_k + Bw w_k
        %     w_k = Delta (Cy x_k + Dyu u_k)
        %     Delta is block diagonal with sparsity defined by delta_blocks
        b_w = [];
        c_y = [];
        d_y_u = [];
        delta_blocks = [];
        
        % Cost matrices
        %     | Q  N | = | Cz'  Cz   Cz' Dz |
        %     | N' R |   | Dzu' Cz  Dzu' Dzu |
        c_z = [];
        d_z_u = [];
        
        % Constraint matrices
        %     Hx x_k + Hu u_k + g <= 0
        h_x = [];
        h_u = [];
        g = [];

        % Terminal constraint matrices
        %     Htx x_k + Htu u_k + gt <= 0
        h_t_x = [];
        g_t = [];
        
        % System dimensions
        n_x = 0;  % Number of states
        n_u = 0;  % Number of control inputs
        n_r = 0;  % Number of reference inputs
        n_w = 0;  % Number of disturbance inputs
        n_y = 0;  % Number of disturbance outputs
        n_b = 0;  % Number of disturbance blocks
        n_z = 0;  % Number of cost outputs (positive eigenvalues of [Q N; N' R])
        n_c = 0;  % Number of constraints
        n_t_c = 0;  % Number of terminal constraints
        n_t = 0;  % GCMPC horizon length
        
        % Linear GCC results
        gcc = struct('k', [], 'p', [], 'x', [], 'r_bar', []);
        
        % Nil potent controller results
        np = struct('k', [], 'a_cl', []);
        
        % mRPI results
        mrpi = struct('q_r_inv', [], 'a_alpha', [], 'a_lambda', [])
        
        % Linear GCRT results
        gcrt = struct('k', [], 'l', [], 'p', [], 'x', [], 'r_bar', []);
        
        % Optimization problem structure
        opt = struct('objective', 0, 'constraint', [], ...
                     'variable', struct('x', [], 'v', [], 'u', []), ...
                     'controller', 0, 'type', '');
        
        % Boolean flags to check if all requirements have been met
        is_system_set                = false;
        is_disturbance_set           = false;
        is_cost_set                  = false;
        is_constraint_set            = false;
        is_constraint_soft           = false;
        is_terminal_constraint_set   = false;
        is_terminal_constraint_soft  = false;
        is_gcc_set                   = false;
        is_gcrt_set                  = false;
        is_nilpotent_set             = false;
        is_mrpi_set                  = false;
        is_reference_set             = false;
        
        % Constants
        kAlphaWeight = 1e2;    % Weight for alpha variable in HTMPC-LDI formulation
        kPosDefTest = 1e-10;   % Minimum eigenvalue to consider matrix Positive-Definite
        kSymTest = 1e-8;       % Maximum difference to transpose for symmetric 
        kZeroTest = 1e-8;      % Maximum absolute value to be considered zero
        kSlackWeight = 1e4;    % Slack weight for soft constraints
        kSdpSolver = 'mosek';  % Default SDP solver
        kQpSolver = 'mosek';   % Default (QC)QP solver
    end
    
    properties(SetAccess=public, GetAccess=public)
        % Options structure
        options = struct('solver_sdp', '', ...
                         'solver_qp', '', ...
                         'use_rlqr', false, ...
                         'use_approximate_mrpi', false);
    end
    
    methods
        function obj = GCMPC()
            % Check if YALMIP is installed
            try
                yalmip('clear');
            catch
                error('YALMIP not found, please install it first')
            end
            
            % Set default solvers, you can change this later
            obj.options.solver_sdp = obj.kSdpSolver;
            obj.options.solver_qp = obj.kQpSolver;
        end
    end
end
