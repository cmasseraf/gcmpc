function controller = generate(obj, n_t, type, is_debug)
%GENERATE Generate GCMPC controller based on desired formulation
%
%    Input(s):
%    (1) obj      - GCMPC class instance
%    (2) n_t      - GCMPC problem horizon
%    (3) type     - String with formulation type
%    (4) is_debug - If true returns debug outputs for solver
%
%    Output(s):
%    (1) controller - GCMPC controller instance
%
%    Author(s):
%    (1) Carlos M. Massera

    if nargin <= 3
        is_debug = false;
    end

    switch type
        case 'OL-GCMPC'
            controller = obj.calculate_olmpc(n_t, is_debug);
        
        case 'T-GCMPC'
            if ~obj.is_reference_set
                controller = obj.calculate_tmpc(n_t, is_debug);
            else
                controller = obj.calculate_rt_tmpc(n_t, is_debug);
            end
            
        case 'HTMPC-LDI'
            controller = obj.calculate_htmpc_ldi(n_t, is_debug);
    end
end

