function obj = set_system(obj, a, b_u, b_r)
%SET_SYSTEM Defines the system matrices A and B for the generated controller
%
%    Input(s):
%    (1) obj - GCMPC class instance
%    (2) a   - State transition matrix
%    (3) b_u - Control input matrix
%    (4) b_r - Reference input matrix (optional)
%
%    Author(s):
%    (1) Carlos M. Massera

    if obj.is_system_set
        warning('System definition is replaced, make sure your code is correct')
    end

    % Get system sizes
    n_x = size(a, 2);
    n_u = size(b_u, 2);
    
    % Check matrix consistency
    if size(a, 1) ~= n_x
        error('A matrix is not square');
    end
    
    if size(b_u, 1) ~= n_x
        error('Bu matrix size does not match A matrix');
    end
    
    % Ensure b_r is set to proper sizes
    if nargin < 4
        b_r = zeros(n_x, 0);
        n_r = 0;
    else
        n_r = size(b_r, 2);
    end
    
    % Set instance variables
    obj.a = a;
    obj.b_u = b_u;
    obj.b_r = b_r;
    obj.n_x = n_x;
    obj.n_u = n_u;
    obj.n_r = n_r;
    obj.is_system_set = true;
    if nargin >= 4
        obj.is_reference_set = true;
    end
end
