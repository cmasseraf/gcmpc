function obj = calculate_gcrt(obj, n_t)
% CALCULATE_GCRT This function calculates the linear Guaranteed Cost Reference Tracking Controller
% with reference preview of size n_t.
%
%    Input(s):
%    (1) obj - GCMPC class instance
%    (2) n_t - GCMPC problem horizon
%
%    Author(s):
%    (1) Carlos M. Massera

    % Check depedencies
    if ~obj.is_system_set
        error('System matrices not set, define them before the generating GCC')
    end
    
    if ~obj.is_disturbance_set
        error('Disturbance matrices not set, define them before the generating GCC')
    end
    
    if ~obj.is_cost_set
        error('Cost matrices not set, define them before the generating GCC')
    end
    
    % Generate GCC controller if it is not generated
    if ~obj.is_gcc_set
        obj.calculate_gcc();
    end
    
    % Create expanded transition matrix
    a_bar = blkvar;
    for i = 1:(n_t + 1)
        for j = 1:(n_t + 1)
            if j == 1
                a_bar(i, j) = obj.a ^ (i - 1);
                continue
            end
            if j <= i
                a_bar(i, j) = obj.a ^ (i - j) * obj.b_r;
                continue
            end
            if j > i
                a_bar(i, j) = zeros(obj.n_x, 1);
                continue
            end
        end
    end
    a_bar = double(sdpvar(a_bar));
    
    % Create expanded control input matrix
    b_u_bar = blkvar;
    for i = 1:(n_t + 1)
        for j = 1:(n_t + 1)
            if j < i
                b_u_bar(i, j) = obj.a ^ (i - j - 1) * obj.b_u;
                continue
            end
            if j >= i
                b_u_bar(i, j) = zeros(obj.n_x, obj.n_u);
                continue
            end
        end
    end
    b_u_bar = double(sdpvar(b_u_bar));
    b_u_bar = b_u_bar(:, 1:obj.n_u * n_t);
    
    % Create expended disturbance input matrix
    b_w_bar = blkvar;
    for i = 1:(n_t + 1)
        for j = 1:(n_t + 1)
            if j < i
                b_w_bar(i, j) = obj.a ^ (i - j - 1) * obj.b_w;
                continue
            end
            if j >= i
                b_w_bar(i, j) = zeros(obj.n_x, obj.n_w);
                continue
            end
        end
    end
    b_w_bar = double(sdpvar(b_w_bar));
    b_w_bar = b_w_bar(:, 1:obj.n_w * n_t);
    
    % Create expanded disturbance output feedback matrices
    c_y_lift = blkdiag(kron(eye(n_t), obj.c_y), zeros(obj.n_x));
    c_y_lift = c_y_lift(1:obj.n_y * n_t, :);
    d_y_u_lift = kron(eye(n_t), obj.d_y_u);

    c_y_bar = c_y_lift * a_bar;
    d_y_u_bar = d_y_u_lift + c_y_lift * b_u_bar;
    d_y_w_bar = c_y_lift * b_w_bar;
    
    %  Create expanded cost output matrices
    c_z_lift = blkdiag(kron(eye(n_t), obj.c_z), chol(obj.gcc.p));
    d_z_u_lift = blkdiag(kron(eye(n_t), obj.d_z_u), zeros(obj.n_x));
    d_z_u_lift = d_z_u_lift(:, 1:obj.n_u * n_t);

    c_z_bar = c_z_lift * a_bar;
    d_z_u_bar = d_z_u_lift + c_z_lift * b_u_bar;
    d_z_w_bar = c_z_lift * b_w_bar;
    
    % Define synthesis variables
    k = sdpvar(obj.n_u * n_t, obj.n_x + obj.n_r * n_t, 'full');
    p_bar = sdpvar(obj.n_x + obj.n_r * n_t, obj.n_x + obj.n_r * n_t);
    if obj.n_u ~= 1 || obj.n_w ~= 1
        l_upsilon_w = blkvar;
    else
        l_upsilon_w = sdpvar(n_t * obj.n_u, n_t * obj.n_w, 'full');
    end
    for i = 1:n_t
        for j = 1:n_t
            if j < i
                l_upsilon_w(i, j) = sdpvar(obj.n_u, obj.n_w, 'full');
            else
                l_upsilon_w(i, j) = zeros(obj.n_u, obj.n_w);
            end
        end
    end
    if obj.n_u ~= 1 || obj.n_w ~= 1
        l_upsilon_w = sdpvar(l_upsilon_w);
    end
    
    % Matrix representation of S-Procedure variables
    e = sdpvar(obj.n_b, 1);
    upsilon_w = [];
    upsilon_y = [];
    for i = 1:obj.n_b
        upsilon_w = blkdiag(upsilon_w, e(i) * eye(obj.delta_blocks(i, 1)));
        upsilon_y = blkdiag(upsilon_y, e(i) * eye(obj.delta_blocks(i, 2)));
    end
    
    % Create expanded S-Procedure variables
    upsilon_w = kron(eye(n_t), upsilon_w);
    upsilon_y = kron(eye(n_t), upsilon_y);
    
    % Problem GCRT synthesis problem LMI
    gcrt_lmi = blkvar;
    gcrt_lmi(1, 1) = - upsilon_y;
    gcrt_lmi(1, 3) = c_y_bar - d_y_u_bar * k;
    gcrt_lmi(1, 4) = d_y_w_bar * upsilon_w - d_y_u_bar * l_upsilon_w;
    gcrt_lmi(2, 2) = - eye(obj.n_x + obj.n_z * n_t);
    gcrt_lmi(2, 3) = c_z_bar - d_z_u_bar * k;
    gcrt_lmi(2, 4) = d_z_w_bar * upsilon_w - d_z_u_bar * l_upsilon_w;
    gcrt_lmi(3, 3) = - p_bar;
    gcrt_lmi(4, 4) = - upsilon_w;
    
    % Define YALMIP optimization problem
    constraints = [gcrt_lmi <= 0];
    objective = trace(p_bar);
    options = sdpsettings('solver', obj.options.solver_sdp, 'verbose', 0);
    
    % Solve optimization problem
    solve_out = optimize(constraints, objective, options);
    
    if solve_out.problem ~= 0
        error('SDP solver did not converge, please check if your problem is correct');
    end
    
    % S-Procedure variable
    upsilon_w = value(upsilon_w);
    upsilon_y = value(upsilon_y);
    
    % GCC cost matrix
    obj.gcrt.p = value(p_bar);
    % GCC gain matrix
    obj.gcrt.k = value(k);
    obj.gcrt.l = value(l_upsilon_w) / upsilon_w;
    % Suboptimal gains, needed to get r_bar
    p_inv = inv(kron(eye(n_t), obj.gcrt.p(1:obj.n_x, 1:obj.n_x)));
    b_w_lift = kron(eye(n_t), obj.b_w);
    % Ensure there are no negative eigenvalues close to zero that shouldn't be there
    x_inv = p_inv - b_w_lift * upsilon_w * b_w_lift';
    [u, s, vt] = svd(x_inv);
    s(s <= 1e-3) = 0;
    s(s > 1e-3) = 1 ./ s(s > 1e-3);
    obj.gcrt.x = u * s * vt';
    % For a controller u = - K x + v, the new cost matrix is v' * r_bar * v
    b_u_lift = kron(eye(n_t), obj.b_u);
    obj.gcrt.r_bar = (d_z_u_lift' * d_z_u_lift) + ...
                     d_y_u_lift' * upsilon_y * d_y_u_lift + ...
                     b_u_lift' * obj.gcrt.x * b_u_lift;
    % Set calculation flag
    obj.is_gcrt_set = true;
    
end