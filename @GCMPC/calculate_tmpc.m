function controller = calculate_tmpc(obj, n_t, is_debug)
%CALCULATE_TMPC This function generates a YALMIP optimizer for the GCMPC problem
%
%    Input(s):
%    (1) obj - GCMPC class instance
%    (2) n_t - GCMPC problem horizon
%    (4) is_debug - If true returns debug outputs for solver
%
%    Output(s):
%    (1) controller - GCMPC controller instance
%
%    Author(s):
%    (1) Carlos M. Massera

    if nargin <= 2
        is_debug = false;
    end

    % Check depedencies
    if ~obj.is_system_set
        error('System matrices not set, define them before the generating GCC')
    end
    
    if ~obj.is_disturbance_set
        error('Disturbance matrices not set, define them before the generating GCC')
    end
    
    if ~obj.is_cost_set
        error('Cost matrices not set, define them before the generating GCC')
    end
    
    if ~obj.is_constraint_set
        warning('Cosntraint matrices not set, assuming system is unconstrained')
    end
    
    % Generate GCC controller and mRPI if they are not generated
    if ~obj.is_gcc_set
        obj.calculate_gcc();
    end
    
    if ~obj.is_mrpi_set
        if ~obj.options.use_approximate_mrpi
            obj.calculate_mrpi();
        else
            obj.calculate_approx_mrpi();
        end
    end
    
    % Set horizon
    obj.n_t = n_t;
    
    % Define optimization variables
    z = sdpvar(obj.n_x, obj.n_t + 1, 'full');
    v = sdpvar(obj.n_u, obj.n_t, 'full');
    alpha = sdpvar(1, obj.n_t + 1, 'full');
    lambda = sdpvar(obj.n_b, obj.n_t, 'full');
    
    u = - obj.gcc.k * z(:, 1:end-1) + v;
    
    % Define problem objective
    p = obj.gcc.p;
    r_bar = obj.gcc.r_bar;
    objective = z(:,1)' * p * z(:,1);
    for i = 1:obj.n_t
        objective = objective + v(:,i)' * r_bar * v(:,i);
    end
    
    % Define system dynamics constraints
    constraint = (z(:,2:end) == (obj.a - obj.b_u * obj.gcc.k) * z(:,1:end-1) + obj.b_u * v);

    % Define robust dynamics
    block_range = [
        0, 0;
        cumsum(obj.delta_blocks, 1);
    ];
    c_y_tilda = obj.c_y - obj.d_y_u * obj.gcc.k;
    c_y_alpha = c_y_tilda * (obj.mrpi.q_r_inv ^ 0.5);
    constraint = [
        constraint;
        alpha >= 0;
        lambda >= 0
    ];

    for i = 1:obj.n_t
        constraint = [
            constraint;
            norm([
                sqrt(obj.mrpi.a_alpha) * alpha(i);
                sqrt(obj.mrpi.a_lambda) .* lambda(:, i)
            ]) <= alpha(i + 1);
        ];
    
        % Define lambda dynamics for each block
        for j = 1:obj.n_b
            r_y = [block_range(j, 2) + 1, block_range(j + 1, 2)];
            c_y_tilda_block = sqrt(obj.mrpi.a_lambda(j)) * c_y_tilda(r_y(1):r_y(2), :);
            d_y_u_block = sqrt(obj.mrpi.a_lambda(j)) * obj.d_y_u(r_y(1):r_y(2), :);
            c_y_alpha_block = sqrt(obj.mrpi.a_lambda(j)) * c_y_alpha(r_y(1):r_y(2), :);
            
            constraint = [
                constraint;
                norm(c_y_tilda_block * z(:, i) + d_y_u_block * v(:, i)) + ...
                    norm(c_y_alpha_block) * alpha(i) <= lambda(j, i);
            ];
        end
    end
    
    % Add robust inequalities to optimization
    h_tilda = obj.h_x - obj.h_u * obj.gcc.k;
    h_alpha = sqrt(sum((h_tilda * (obj.mrpi.q_r_inv ^ 0.5)) .^ 2, 2));
    h_value = h_tilda * z(:,1:end-1) + obj.h_u * v + obj.g * ones(1, obj.n_t);
    
    if ~obj.is_constraint_soft  % Hard constraints
        constraint = [
            constraint;
            h_value + h_alpha * alpha(1:end-1) <= 0;
        ];
    else                        % Soft constraints
        slack = sdpvar(obj.n_c, obj.n_t);
        
        % Objective
        objective = objective + obj.kSlackWeight * sum(sum(slack));
        
        % Constraint slacking
        constraint = [
            constraint;
            h_value + h_alpha * alpha(1:end-1) <= slack;
            slack >= 0;
        ];
    end

    if obj.is_terminal_constraint_set
        h_t_alpha = sqrt(sum((obj.h_t_x * (obj.mrpi.q_r_inv ^ 0.5)) .^ 2, 2));
        h_t_value = obj.h_t_x * z(:,end) + obj.g_t;

        if ~obj.is_terminal_constraint_soft  % Hard constraints
            constraint = [
                constraint;
                h_t_value + h_t_alpha * alpha(end) <= 0
            ];
        else                        % Soft constraints
            slack = sdpvar(obj.n_t_c, 1);

            % Objective
            objective = objective + obj.kSlackWeight * sum(slack);

            constraint = [
                constraint;
                h_t_value + h_t_alpha * alpha(end) <= slack;
                slack >= 0;
            ];
        end
    end
    
    % If debug mode is enabled, output (almost) all internal variables
    if ~is_debug
        outputs = {u(:, 1), objective};
    else
        % Add minimal cost to alpha as it is relaxed but we still want a
        % minimal value if we are plotting
        objective = objective + alpha(end);
        outputs = {z, u, alpha, lambda, objective};
    end
    
    % Create YALMIP object
    ops = sdpsettings('solver', obj.options.solver_qp, 'verbose', 0);
    controller = optimizer(constraint, objective, ops, z(:,1), outputs);

    % Save everything else
    obj.opt.objective = objective;
    obj.opt.constraint = constraint;
    obj.opt.variable.x = z;
    obj.opt.variable.u = u;
    obj.opt.variable.v = v;
    obj.opt.controller = controller;
    obj.opt.type = 'T-GCMPC';
end