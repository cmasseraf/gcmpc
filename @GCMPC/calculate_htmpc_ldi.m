function controller = calculate_htmpc_ldi(obj, n_t, is_debug)
%CALCULATE_HTMPC_LDI This function generates a YALMIP optimizer for the HTMPC problem
%    See https://ieeexplore.ieee.org/document/6561023 for details
%
%    Input(s):
%    (1) obj - GCMPC class instance
%    (2) n_t - GCMPC problem horizon
%    (4) is_debug - If true returns debug outputs for solver
%
%    Output(s):
%    (1) controller - GCMPC controller instance
%
%    Author(s):
%    (1) Carlos M. Massera

    if nargin <= 2
        is_debug = false;
    end
    
    % HTMPC only works if disturbance can be made into linear difference inclusion
    % e.g. if delta is diagonal
    if obj.delta_blocks ~= ones(size(obj.delta_blocks))
        error('Uncertainty is not diagonal, use T-GCMPC instead')
    end

    % Check depedencies
    if ~obj.is_system_set
        error('System matrices not set, define them before the generating GCC')
    end
    
    if ~obj.is_disturbance_set
        error('Disturbance matrices not set, define them before the generating GCC')
    end
    
    if ~obj.is_cost_set
        error('Cost matrices not set, define them before the generating GCC')
    end
    
    if ~obj.is_constraint_set
        warning('Cosntraint matrices not set, assuming system is unconstrained')
    end
    
    % Generate GCC controller and mRPI if they are not generated
    if ~obj.is_gcc_set
        obj.calculate_gcc();
    end
    
    if ~obj.is_mrpi_set
        if ~obj.options.use_approximate_mrpi
            obj.calculate_mrpi();
        else
            obj.calculate_approx_mrpi();
        end
    end
    
    % Enumarate vertexes of A and B
    a = zeros(size(obj.a));
    b_u = zeros(size(obj.b_u));
    disturbance_edges = delta_vector(obj.n_w);
    n_systems = size(disturbance_edges, 1);
    for i = 1 : n_systems
        delta = diag(disturbance_edges(i, :));
        a(:, :, i) = obj.a + obj.b_w * delta * obj.c_y;
        b_u(:, :, i) = obj.b_u + obj.b_w * delta * obj.d_y_u;
    end
    
    % Set horizon
    obj.n_t = n_t;
    
    % Define optimization variables
    z = sdpvar(obj.n_x, obj.n_t + 1, 'full');
    v = sdpvar(obj.n_u, obj.n_t, 'full');
    alpha = sdpvar(1, obj.n_t + 1, 'full');
    
    % Define problem objective
    p = obj.gcc.p;
    w_half = [obj.c_z, obj.d_z_u];
    w = w_half' * w_half;
    objective = 0;
    for i = 1:obj.n_t
        objective = objective + [z(:,i); v(:, i)]' * w * [z(:, i); v(:,i)] + obj.kAlphaWeight * alpha(i)^2;
    end
    objective = objective + z(:, obj.n_t + 1)' * p * z(:, obj.n_t + 1) + obj.kAlphaWeight * alpha(obj.n_t + 1)^2;
    
    % Define system dynamics constraints
    constraint = [];
    e_r_half = obj.mrpi.q_r_inv ^ -0.5;
    for i = 1 : n_systems
        for j = 1 : obj.n_t
            a_tilda_i = a(:, :, i) - b_u(:, :, i) * obj.gcc.k;
            a_alpha = norm(e_r_half * a_tilda_i / e_r_half, 2);
            dynamics_error = a(:, :, i) * z(:,j) + b_u(:, :, i) * v(:, j) - z(:,j + 1);
            constraint = [
                constraint;
                a_alpha * alpha(j) + norm(e_r_half * dynamics_error, 2) <= alpha(j + 1);
            ];
        end
    end

    constraint = [
        constraint;
        alpha >= 0;
    ];
    
    % Add robust inequalities to optimization
    h_tilda = obj.h_x - obj.h_u * obj.gcc.k;
    h_alpha = sqrt(sum((h_tilda * (obj.mrpi.q_r_inv ^ 0.5)) .^ 2, 2));
    h_value = obj.h_x * z(:,1:end-1) + obj.h_u * v + obj.g * ones(1, obj.n_t);
    
    if ~obj.is_constraint_soft  % Hard constraints
        constraint = [
            constraint;
            h_value + h_alpha * alpha(1:end-1) <= 0;
        ];
    else                        % Soft constraints
        slack = sdpvar(obj.n_c, obj.n_t);
        
        % Objective
        objective = objective + obj.kSlackWeight * sum(sum(slack));
        
        % Constraint slacking
        constraint = [
            constraint;
            h_value + h_alpha * alpha(1:end-1) <= slack;
            slack >= 0;
        ];
    end

    if obj.is_terminal_constraint_set
        h_t_alpha = sqrt(sum((obj.h_t_x * (obj.mrpi.q_r_inv ^ 0.5)) .^ 2, 2));
        h_t_value = obj.h_t_x * z(:,end) + obj.g_t;

        if ~obj.is_terminal_constraint_soft  % Hard constraints
            constraint = [
                constraint;
                h_t_value + h_t_alpha * alpha(end) <= 0
            ];
        else                        % Soft constraints
            slack = sdpvar(obj.n_t_c, 1);

            % Objective
            objective = objective + obj.kSlackWeight * sum(slack);

            constraint = [
                constraint;
                h_t_value + h_t_alpha * alpha(end) <= slack;
                slack >= 0;
            ];
        end
    end
    
    % If debug mode is enabled, output (almost) all internal variables
    if ~is_debug
        outputs = {v(:, 1), objective};
    else
        % Add minimal cost to alpha as it is relaxed but we still want a
        % minimal value if we are plotting
        objective = objective;
        outputs = {z, v, alpha, objective};
    end
    
    % Create YALMIP object
    ops = sdpsettings('solver', obj.options.solver_qp, 'verbose', 0);
    controller = optimizer(constraint, objective, ops, z(:,1), outputs);

    % Save everything else
    obj.opt.objective = objective;
    obj.opt.constraint = constraint;
    obj.opt.variable.x = z;
    obj.opt.variable.u = v;
    obj.opt.variable.v = v;
    obj.opt.controller = controller;
    obj.opt.type = 'T-GCMPC';
end

function deltas = delta_vector(k)
    values = [-1, 1];
    
    n = numel(values);
    combs = bsxfun(@minus, nchoosek(1:n+k-1,k), 0:k-1);
    combs = reshape(values(combs),[],k);
    
    deltas = [];
    for i = 1 : size(combs, 1)
        deltas = [deltas; unique(perms(combs(i, :)), 'rows')];
    end
end