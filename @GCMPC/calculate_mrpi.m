function obj = calculate_mrpi(obj)
%CALCULATE_MRPI This function generates the minimum RPI set in central form
%
%    Input(s):
%    (1) obj - GCMPC class instance
%
%    Author(s):
%    (1) Carlos M. Massera

    % Check depedencies
    if ~obj.is_system_set
        error('System matrices not set, define them before the generating mRPI')
    end
    
    if ~obj.is_disturbance_set
        error('Disturbance matrices not set, define them before the generating mRPI')
    end
    
    if ~obj.is_gcc_set
        error('Linear controller not defined, generate GCC before mRPI')
    end
    
    % Define LMI variables
    q_r = sdpvar(obj.n_x, obj.n_x);
    tau_x = sdpvar();
    tau_w = sdpvar(obj.n_b, 1);
    
    % Matrix representation of S-Procedure variables
    lambda_w = [];
    for i = 1:obj.n_b
        lambda_w = blkdiag(lambda_w, tau_w(i) * eye(obj.delta_blocks(i, 1)));
    end
    
    % Define RPI requirement LMI
    rpi_lmi = blkvar;
    rpi_lmi(1,1) = - q_r;
    rpi_lmi(1,2) = q_r * (obj.a - obj.b_u * obj.gcc.k);
    rpi_lmi(1,3) = q_r * obj.b_w;
    rpi_lmi(2,2) = - tau_x * q_r;
    rpi_lmi(3,3) = - lambda_w;
    
    % Define constraints
    constraints = [
        rpi_lmi <= 0;
        tau_w >= 0;
        tau_x + sum(tau_w) <= 1;
    ];

    % Outer-bounding constraint LMI for each disturbance block
    block_range = [
        0, 0;
        cumsum(obj.delta_blocks, 1);
    ];

    for i = 1:obj.n_b 
        r_y = [block_range(i, 2) + 1, block_range(i + 1, 2)];
        c_y_cl = obj.c_y(r_y(1):r_y(2), :) - obj.d_y_u(r_y(1):r_y(2), :) * obj.gcc.k;
        constraints = [
            constraints;
            c_y_cl' * c_y_cl - q_r <= 0;
        ];
    end
    
    % Define YALMIP optimization problem
    objective = - geomean(q_r);
    options = sdpsettings('solver', ['+' obj.options.solver_sdp], 'verbose', 0);
    
    % Solve optimization problem
    opt = optimizer(constraints, objective, options, tau_x, {q_r, tau_w});
    
    tau_list = [];
    cost_list = [];
    q_r_inv_list = [];
    for tau = [0:0.01:1]
        [opt_out, status] = opt(tau);
        
        % Ensure the problem is well posed. Since we use q_r ^ -1 we can't
        % have eigenvalues close to zero.
        if status == 0 && min(eig(opt_out{1})) > 1e-3
            tau_list(:, end+1) = [tau, opt_out{2}'];
            cost_list(end+1) = - logdet(opt_out{1});
            if size(q_r_inv_list, 1) == 0
                q_r_inv_list = inv(opt_out{1});
            else
                q_r_inv_list(:, :, end+1) = inv(opt_out{1});
            end
        end
    end
    
    [~, min_idx] = min(cost_list);
    if size(min_idx, 1) ~= 0
        obj.mrpi.q_r_inv = q_r_inv_list(:, :, min_idx);
        obj.mrpi.a_alpha = tau_list(1, min_idx);
        obj.mrpi.a_lambda = tau_list(2:end, min_idx);
    else
        warning('Exact mRPI is infeasible or ill posed, executing approximate mRPI')
        obj.calculate_approx_mrpi();
    end
        
end