%% System dynamics
system = struct();
system.a = [1.1 0   0;
              0 0 1.2;
             -1 1   0];
system.b_u = [ 0 1;
               1 1;
              -1 0];
system.b_w = [ 0.1  0.1; 
               0.1 -0.1; 
              -0.2  0.1];
system.c_y = [ 0.4123    0.4318   -0.5028
              -0.0061   -0.3218    0.4441];
system.d_y_u = [0.4 -0.4;
                  0  0];
system.delta_blocks = ones(2, 2);  % Disturbance is diagonal

%% Systems constraints
system.constraints = struct();
system.constraints.h_x = [ 1  0  0; 
                          -1  0  0; 
                           0  1  0; 
                           0 -1  0; 
                           0  0  1; 
                           0  0 -1;
                          zeros(4, 3)];
system.constraints.h_u = [zeros(6,2);
                           1  0; 
                          -1  0; 
                           0  1; 
                           0 -1];
system.constraints.g = - ones(10,1);

%% Controller parameters
controller_params = struct();
controller_params.t = 4;
controller_params.q = eye(3);
controller_params.r = eye(2);

%% Create Open-loop and Tube GCMPC
disp('GCMPC synthesis.')
disp(' ')

gcmpc = GCMPC;
gcmpc.set_system(system.a, system.b_u);
gcmpc.set_disturbance(system.b_w, system.c_y, system.d_y_u, system.delta_blocks);
gcmpc.set_constraint(system.constraints.h_x, system.constraints.h_u, system.constraints.g);
gcmpc.set_cost(controller_params.q, controller_params.r);
gcmpc.set_terminal_constraint(system.constraints.h_x, system.constraints.g);

gcmpc.options.use_approximate_mrpi = true;

ol_gcmpc = gcmpc.generate(controller_params.t, 'OL-GCMPC');
t_gcmpc = gcmpc.generate(controller_params.t, 'T-GCMPC');
htmpc_ldi = gcmpc.generate(controller_params.t, 'HTMPC-LDI');

%% Testing
n = 20;
deltas = 2 * rand(n, size(system.b_w, 2), size(system.c_y, 1)) - 1;

%% Test OL-GCMPC
disp('Running OL-GCMPC.')

xs = NaN * ones(n+1, size(system.a,2));
us = NaN * ones(n, size(system.b_u,2));
alphas = NaN * ones(n, controller_params.t);
objs = NaN * ones(n, 1);

ol_gcmpc(zeros(size(system.a,2), 1));

xs(1,:) = 0.7 * [1;-1;1];

for i = 1:n
    tic
    out = ol_gcmpc{xs(i,:)'};
    toc
    
    us(i, :) = out{1}';
    objs(i) = out{2};
    
    delta = reshape(deltas(i, :, :), [size(system.b_w, 2), size(system.c_y, 1)]);
    xs(i+1,:) = ((system.a + system.b_w * delta * system.c_y) * xs(i,:)' + ...
                 (system.b_u + system.b_w * delta * system.d_y_u) * us(i,:)')';
end

xs_ol = xs;
us_ol = us;
objs_ol = objs;
disp(' ')

%% Test T-GCMPC
disp('Running T-GCMPC.')

xs = NaN * ones(n+1, size(system.a,2));
us = NaN * ones(n, size(system.b_u,2));
objs = NaN * ones(n, 1);

t_gcmpc(zeros(size(system.a,2), 1));

xs(1,:) = 0.7 * [1;-1;1];

for i = 1:n
    tic
    out = t_gcmpc{xs(i,:)'};
    toc
    
    us(i, :) = out{1}';
    objs(i) = out{2};
    
    delta = reshape(deltas(i, :, :), [size(system.b_w, 2), size(system.c_y, 1)]);
    xs(i+1,:) = ((system.a + system.b_w * delta * system.c_y) * xs(i,:)' + ...
                 (system.b_u + system.b_w * delta * system.d_y_u) * us(i,:)')';
end

xs_t = xs;
us_t = us;
objs_t = objs;
disp(' ')

%% Test HTMPC-LDI
disp('Running HTMPC-LDI.')

xs = NaN * ones(n+1, size(system.a,2));
us = NaN * ones(n, size(system.b_u,2));
objs = NaN * ones(n, 1);

t_gcmpc(zeros(size(system.a,2), 1));

xs(1,:) = 0.7 * [1;-1;1];

for i = 1:n
    tic
    out = htmpc_ldi{xs(i,:)'};
    toc
    
    us(i, :) = out{1}';
    objs(i) = out{2};
    
    delta = reshape(deltas(i, :, :), [size(system.b_w, 2), size(system.c_y, 1)]);
    xs(i+1,:) = ((system.a + system.b_w * delta * system.c_y) * xs(i,:)' + ...
                 (system.b_u + system.b_w * delta * system.d_y_u) * us(i,:)')';
end

xs_h = xs;
us_h = us;
objs_h = objs;
disp(' ')

%% Plot OL-GCMPC results
columnwidth = 8.63;
textwidth = 17.78;

fig = figure(1);
clf
set(gca, 'FontSize', 8)

subplot(1, 3, 1)
hold on
plot([0:length(xs_ol) - 1], xs_ol(:, 1), 'b')
plot([0:length(xs_ol) - 1], xs_ol(:, 2), 'r--')
plot([0:length(xs_ol) - 1], xs_ol(:, 3), 'g-.')
grid on
xlabel('Timestep - t')
ylabel('State - x')
legend('State 1', 'State 2', 'State 3')
xlim([0, 10])
subplot(1, 3, 2)
hold on
plot([0:length(us_t) - 1], us_ol(:, 1), 'b')
plot([0:length(us_t) - 1], us_ol(:, 2), 'r--')
grid on
xlabel('Timestep - t')
ylabel('Control Input - u')
legend('Input 1', 'Input 2')
xlim([0, 10])
subplot(1, 3, 3)
plot([0:length(us_ol) - 1], reshape(deltas, [size(deltas, 1), size(system.b_w, 2) * size(system.c_y, 1)]))
grid on
xlabel('Timestep - t')
ylabel('Disturbance - \delta')
xlim([0, 10])

fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 1.5 * textwidth 0.75 * columnwidth];
fig.PaperSize = [1.5 * textwidth 0.75 * columnwidth];

print -dpng -r300 example_olgcmpc.png

%% Plot T-GCMPC results
fig = figure(2);
clf

subplot(1, 3, 1)
set(gca, 'FontSize', 8)
hold on
plot([0:length(xs_t) - 1], xs_t(:, 1), 'b')
plot([0:length(xs_t) - 1], xs_t(:, 2), 'r--')
plot([0:length(xs_t) - 1], xs_t(:, 3), 'g-.')
grid on
xlabel('Timestep - t')
ylabel('State - x')
legend('State 1', 'State 2', 'State 3')
xlim([0, 10])
subplot(1, 3, 2)
set(gca, 'FontSize', 8)
hold on
plot([0:length(us_t) - 1], us_t(:, 1), 'b')
plot([0:length(us_t) - 1], us_t(:, 2), 'r--')
grid on
xlabel('Timestep - t')
ylabel('Control Input - u')
legend('Input 1', 'Input 2', 'location', 'SouthEast')
xlim([0, 10])
subplot(1, 3, 3)
set(gca, 'FontSize', 8)
hold on
plot([0:length(us_t) - 1], reshape(deltas, [size(deltas, 1), size(system.b_w, 2) * size(system.c_y, 1)]))
grid on
xlabel('Timestep - t')
ylabel('Disturbance - \delta')
xlim([0, 10])

fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 textwidth 0.5 * columnwidth];
fig.PaperSize = [textwidth 0.5 * columnwidth];

print -dpng -r300 example_tgcmpc.png

%% Plot HTMPC-LDI results
fig = figure(3);
clf

subplot(1, 3, 1)
set(gca, 'FontSize', 8)
hold on
plot([0:length(xs_h) - 1], xs_h(:, 1), 'b')
plot([0:length(xs_h) - 1], xs_h(:, 2), 'r--')
plot([0:length(xs_h) - 1], xs_h(:, 3), 'g-.')
grid on
xlabel('Timestep - t')
ylabel('State - x')
legend('State 1', 'State 2', 'State 3')
xlim([0, 10])
subplot(1, 3, 2)
set(gca, 'FontSize', 8)
hold on
plot([0:length(us_h) - 1], us_h(:, 1), 'b')
plot([0:length(us_h) - 1], us_h(:, 2), 'r--')
grid on
xlabel('Timestep - t')
ylabel('Control Input - u')
legend('Input 1', 'Input 2', 'location', 'SouthEast')
xlim([0, 10])
subplot(1, 3, 3)
set(gca, 'FontSize', 8)
hold on
plot([0:length(us_h) - 1], reshape(deltas, [size(deltas, 1), size(system.b_w, 2) * size(system.c_y, 1)]))
grid on
xlabel('Timestep - t')
ylabel('Disturbance - \delta')
xlim([0, 10])

fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 textwidth 0.5 * columnwidth];
fig.PaperSize = [textwidth 0.5 * columnwidth];

print -dpng -r300 example_htmpc_ldi.png

%% Plot 3D tube result
t_gcmpc = gcmpc.generate(20, 'T-GCMPC', true);

x_0 = 0.6 * [1, -1, 1]';
[out, status, msg] = t_gcmpc{x_0};

x = out{1};
alpha = out{3};
n = size(x, 2);

fig = figure(4);
clf
set(gca, 'FontSize', 8)

hold on
plot(x(1, :), x(2, :), 'b')
for i = 1:n
    e_r = gcmpc.mrpi.q_r_inv ^ -1 / (alpha(i) ^ 2 + 1e-5);
    plot_ellipse(e_r(1:2, 1:2) , x(1:2, i), 'r', 0.2)
end

xlabel('State 1 - x_1')
ylabel('State 2 - x_2')
zlabel('State 3 - x_3')

xlim([-0.8, 0.8])
ylim([-1, 0.2])

legend('Nominal states', 'Robust tube', 'location', 'SouthEast')

fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 columnwidth 0.7 * columnwidth];
fig.PaperSize = [columnwidth 0.7 * columnwidth];

print -dpng -r300 tgcmpc_tube.png

%% Plot 3D tube result
htmpc_ldi = gcmpc.generate(20, 'HTMPC-LDI', true);

x_0 = 0.6 * [1, -1, 1]';
[out, status, msg] = htmpc_ldi{x_0};

x = out{1};
alpha = out{3};
n = size(x, 2);

fig = figure(5);
clf
set(gca, 'FontSize', 8)

hold on
plot(x(1, :), x(2, :), 'b')
for i = 1:n
    e_r = gcmpc.mrpi.q_r_inv ^ -1 / (alpha(i) ^ 2 + 1e-5);
    plot_ellipse(e_r(1:2, 1:2) , x(1:2, i), 'r', 0.2)
end

xlabel('State 1 - x_1')
ylabel('State 2 - x_2')

xlim([-0.8, 0.8])
ylim([-1, 0.2])

legend('Nominal states', 'Robust tube', 'location', 'SouthEast')

fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 columnwidth 0.7 * columnwidth];
fig.PaperSize = [columnwidth 0.7 * columnwidth];

print -dpng -r300 htmpc_ldi_tube.png