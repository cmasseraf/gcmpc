%% System dynamics
system = struct();
system.a = [1.1 0   0;
              0 0 1.2;
             -1 1   0];
system.b_u = [ 0 1;
               1 1;
              -1 0];
system.b_w = [ 0.1  0.1; 
               0.1 -0.1; 
              -0.2  0.1];
system.c_y = [ 0.4123    0.4318   -0.5028
              -0.0061   -0.3218    0.4441];
system.d_y_u = [0.4 -0.4;
                  0  0];
system.delta_blocks = ones(2, 2);  % Disturbance is diagonal

%% Systems constraints
system.constraints = struct();
system.constraints.h_x = [ 1  0  0; 
                          -1  0  0; 
                           0  1  0; 
                           0 -1  0; 
                           0  0  1; 
                           0  0 -1;
                          zeros(4, 3)];
system.constraints.h_u = [zeros(6,2);
                           1  0; 
                          -1  0; 
                           0  1; 
                           0 -1];
system.constraints.g = - ones(10,1);

%% Controller parameters
controller_params = struct();
controller_params.t = 5;
controller_params.q = eye(3);
controller_params.r = eye(2);

%% Create Open-loop and Tube GCMPC
disp('GCMPC synthesis.')
disp(' ')

gcmpc = GCMPC;
gcmpc.set_system(system.a, system.b_u);
gcmpc.set_disturbance(system.b_w, system.c_y, system.d_y_u, system.delta_blocks);
gcmpc.set_constraint(system.constraints.h_x, system.constraints.h_u, system.constraints.g);
gcmpc.set_cost(controller_params.q, controller_params.r);
gcmpc.set_terminal_constraint(system.constraints.h_x, system.constraints.g);

gcmpc.options.use_approximate_mrpi = false;

ol_gcmpc = gcmpc.generate(controller_params.t, 'OL-GCMPC');
t_gcmpc = gcmpc.generate(controller_params.t, 'T-GCMPC');
htmpc_ldi = gcmpc.generate(controller_params.t, 'HTMPC-LDI');

%% Testing
n = 20;
deltas = 2 * rand(n, size(system.b_w, 2), size(system.c_y, 1)) - 1;

%% Test outter loop
lambda = [0.01:0.01:1];
cost_over_lambda = [];

for k = 1:length(lambda)
    %% Test OL-GCMPC
    disp('Running OL-GCMPC.')

    xs = NaN * ones(n+1, size(system.a,2));
    us = NaN * ones(n, size(system.b_u,2));
    objs = NaN * ones(n, 1);

    ol_gcmpc(zeros(size(system.a,2), 1));

    xs(1,:) = lambda(k) * [1;-1;1];

    for i = 1:n
        tic
        out = ol_gcmpc{xs(i,:)'};
        toc

        us(i, :) = out{1}';
        objs(i) = out{2};

        delta = reshape(deltas(i, :, :), [size(system.b_w, 2), size(system.c_y, 1)]);
        xs(i+1,:) = ((system.a + system.b_w * delta * system.c_y) * xs(i,:)' + ...
                     (system.b_u + system.b_w * delta * system.d_y_u) * us(i,:)')';
    end

    xs_ol = xs;
    us_ol = us;
    objs_ol = objs;
    disp(' ')

    %% Test T-GCMPC
    disp('Running T-GCMPC.')

    xs = NaN * ones(n+1, size(system.a,2));
    us = NaN * ones(n, size(system.b_u,2));
    objs = NaN * ones(n, 1);

    t_gcmpc(zeros(size(system.a,2), 1));

    xs(1,:) = lambda(k) * [1;-1;1];

    for i = 1:n
        tic
        out = t_gcmpc{xs(i,:)'};
        toc

        us(i, :) = out{1}';
        objs(i) = out{2};

        delta = reshape(deltas(i, :, :), [size(system.b_w, 2), size(system.c_y, 1)]);
        xs(i+1,:) = ((system.a + system.b_w * delta * system.c_y) * xs(i,:)' + ...
                     (system.b_u + system.b_w * delta * system.d_y_u) * us(i,:)')';
    end

    xs_t = xs;
    us_t = us;
    objs_t = objs;
    disp(' ')
    
    %% Test HTMPC-LDI
    disp('Running HTMPC-LDI')

    xs = NaN * ones(n+1, size(system.a,2));
    us = NaN * ones(n, size(system.b_u,2));
    objs = NaN * ones(n, 1);

    htmpc_ldi(zeros(size(system.a,2), 1));

    xs(1,:) = lambda(k) * [1;-1;1];

    for i = 1:n
        tic
        out = htmpc_ldi{xs(i,:)'};
        toc

        us(i, :) = out{1}';
        objs(i) = out{2};

        delta = reshape(deltas(i, :, :), [size(system.b_w, 2), size(system.c_y, 1)]);
        xs(i+1,:) = ((system.a + system.b_w * delta * system.c_y) * xs(i,:)' + ...
                     (system.b_u + system.b_w * delta * system.d_y_u) * us(i,:)')';
    end

    xs_h = xs;
    us_h = us;
    objs_h = objs;
    disp(' ')
    
    %% Save t = 0 costs
    cost_over_lambda(k, :) = [objs_ol(1), objs_t(1), objs_h(1)];
end

%% Plot cost results
columnwidth = 8.63;
textwidth = 17.78;

fig = figure(1);
clf

% hold on
semilogy(lambda, cost_over_lambda(:, 2))
set(gca, 'FontSize', 8)
hold on
semilogy(lambda, cost_over_lambda(:, 1), 'r--')
semilogy(lambda, cost_over_lambda(:, 3), 'g-.')
grid on
[~, midx] = max(cost_over_lambda(:, 1));
plot(lambda(midx), cost_over_lambda(midx, 1), 'rx', 'MarkerSize', 12)
[~, midx] = max(cost_over_lambda(:, 2));
plot(lambda(midx), cost_over_lambda(midx, 2), 'bx', 'MarkerSize', 12)
[~, midx] = max(cost_over_lambda(:, 3));
plot(lambda(midx), cost_over_lambda(midx, 3), 'gx', 'MarkerSize', 12)
xlabel('Scalling factor - \lambda [-]')
ylabel('Optimal cost - J^*(x_0) [-]')
legend('T-GCMPC', 'OL-GCMPC', 'HTMPC', 'Location', 'SouthEast')

fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 columnwidth 0.66 * columnwidth];
fig.PaperSize = [columnwidth 0.66 * columnwidth];

print -dpng -r300 gcmpc_cost.png