clear all
close all

%% System dynamics
system = struct();
system.a = [ 1 -0.1 0.1;
             0 1   0;
             0 0   1];
system.b_u = [-0.005;
              0.1;
              0];
system.b_r = [0.005;
              0;
              0.1];
system.b_w = 0.2 * system.b_u;
system.c_y = [0 0 0];
system.d_y_u = [1];

%% Systems constraints
system.constraints = struct();
system.constraints.h_x = [-1  0.5  0; 
                           0 -1    0;
                           0  1    0;
                           0  0    0;
                           0  0    0];
system.constraints.h_u = [0; 0; 0; 1; -1];
system.constraints.g = [0; 0; -30; -1.5; -3.5];

%% Controller parameters
c_z = [1 -1.5 0; 0 0 0];
d_z_u = [0; 1];
controller_params = struct();
controller_params.t = 10;
controller_params.q = c_z' * c_z;
controller_params.n = c_z' * d_z_u;
controller_params.r = d_z_u' * d_z_u;

%% Create Open-loop and Tube GCMPC
disp('GCMPC synthesis.')
disp(' ')

gcmpc = GCMPC;
gcmpc.set_system(system.a, system.b_u, system.b_r);
gcmpc.set_disturbance(system.b_w, system.c_y, system.d_y_u);
gcmpc.set_constraint(system.constraints.h_x, system.constraints.h_u, system.constraints.g);
gcmpc.set_cost(controller_params.q, controller_params.r, controller_params.n);
gcmpc.set_terminal_constraint(system.constraints.h_x, system.constraints.g);

gcmpc.options.use_approximate_mrpi = false;

t_gcmpc = gcmpc.generate(controller_params.t, 'T-GCMPC');

%% Testing
n = 100;
delta_tau = 0.01;
delta_size = size(system.b_w, 2) * size(system.c_y, 1);
deltas = 2 * rand(n, delta_size) - 1;
deltas = lsim( ...
    ss(-delta_tau * eye(delta_size), delta_tau * eye(delta_size), eye(delta_size), 0), ...
    deltas, ...
    0:n-1 ...
);
deltas = 2 * (deltas - min(deltas)) ./ (max(deltas) - min(deltas)) - 1;

%% Test T-GCMPC
disp('Running T-GCMPC.')

xs = NaN * ones(n+1, size(system.a,2));
us = NaN * ones(n, size(system.b_u,2));
objs = NaN * ones(n, 1);

t_gcmpc(zeros(size(system.a,2), 1), zeros(size(system.b_r, 2), controller_params.t));

xs(1,:) = [20;10;10];
r = 1.0 * [ones(n / 2, 1); zeros(n / 2 + controller_params.t, 1)];

for i = 1:n
    tic
    [out, is_successful] = t_gcmpc(xs(i,:)', r(i:(i + controller_params.t - 1), :)');
    toc
    
    assert(is_successful == 0);
    
    us(i, :) = out{1}';
    objs(i) = out{2};
    
    delta = reshape(deltas(i, :), [size(system.b_w, 2), size(system.c_y, 1)]);
    xs(i+1,:) = ((system.a + system.b_w * delta * system.c_y) * xs(i,:)' + ...
                 (system.b_u + system.b_w * delta * system.d_y_u) * us(i,:)' + ...
                 (system.b_r) * r(i,:)')';
end

xs_t = xs;
us_t = us;
objs_t = objs;
disp(' ')

%% Plot cost functionals
figure
clf
grid on
hold on
plot(objs_t, 'b')
legend('T-GCMPC')
xlabel('Timestep - t')
ylabel('Cost functional - J(x)')

%% Plot T-GCMPC results
figure
clf
subplot(2, 2, 1)
plot(xs_t(:, 1))
grid on
xlim([0, n])
xlabel('Timestep - t')
ylabel('Distance - d [m]')
subplot(2, 2, 2)
hold on
plot(xs_t(:, 2))
plot(xs_t(:, 3), 'r--')
grid on
xlim([0, n])
xlabel('Timestep - t')
ylabel('Velocity - v [m/s]')
legend('Ego vehicle - v_e', 'Preceding vehicle - v_p')
subplot(2, 2, 3)
hold on
plot(us_t(:, 1))
plot(r(1:n, 1), 'r--')
grid on
xlim([0, n])
xlabel('Timestep - t')
ylabel('Acceleration - a [m/s^2]')
legend('Ego vehicle - a_e', 'Preceding vehicle - a_p')
subplot(2, 2, 4)
plot(reshape(deltas, [size(deltas, 1), size(system.b_w, 2) * size(system.c_y, 1)]))
grid on
xlim([0, n])
xlabel('Timestep - t')
ylabel('Disturbance - \delta')