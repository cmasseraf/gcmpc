%% System dynamics
system = struct();
system.a = [1.1 0   0;
              0 0 1.2;
             -1 1   0];
system.b_u = [ 0 1;
               1 1;
              -1 0];
system.b_w = [ 0.1  0.1; 
               0.1 -0.1; 
              -0.2  0.1];
system.c_y = [ 0.4123    0.4318   -0.5028
              -0.0061   -0.3218    0.4441];
system.d_y_u = [0.4 -0.4;
                  0  0];
system.delta_blocks = ones(2, 2);  % Disturbance is diagonal

%% Systems constraints
system.constraints = struct();
system.constraints.h_x = [ 1  0  0; 
                          -1  0  0; 
                           0  1  0; 
                           0 -1  0; 
                           0  0  1; 
                           0  0 -1;
                          zeros(4, 3)];
system.constraints.h_u = [zeros(6,2);
                           1  0; 
                          -1  0; 
                           0  1; 
                           0 -1];
system.constraints.g = - ones(10,1);

%% Controller parameters
controller_params = struct();
controller_params.t = 5;
controller_params.q = eye(3);
controller_params.r = eye(2);

%% Create Open-loop and Tube GCMPC
disp('GCMPC synthesis.')
disp(' ')

gcmpc = GCMPC;
gcmpc.set_system(system.a, system.b_u);
gcmpc.set_disturbance(system.b_w, system.c_y, system.d_y_u, system.delta_blocks);
gcmpc.set_constraint(system.constraints.h_x, system.constraints.h_u, system.constraints.g);
gcmpc.set_cost(controller_params.q, controller_params.r);
gcmpc.set_terminal_constraint(system.constraints.h_x, system.constraints.g);

gcmpc.options.use_approximate_mrpi = false;

ol_gcmpc = gcmpc.generate(controller_params.t, 'OL-GCMPC');
t_gcmpc = gcmpc.generate(controller_params.t, 'T-GCMPC');
htmpc_ldi = gcmpc.generate(controller_params.t, 'HTMPC-LDI');

%% Testing
n = 10000;
xs = 0.3 * (2 * rand(n, size(system.a, 2)) - 1);

%% Test OL-GCMPC
ol_gcmpc_time = [];

% Call once to avoid loading libraries during measured execution time
ol_gcmpc(zeros(size(system.a,2), 1));

for k = 1:n
    tic
    ol_gcmpc(xs(k, :)');
    ol_gcmpc_time(k) = toc;
end

%% Test T-GCMPC
t_gcmpc_time = [];

% Call once to avoid loading libraries during measured execution time
t_gcmpc(zeros(size(system.a,2), 1));

for k = 1:n
    tic
    t_gcmpc(xs(k, :)');
    t_gcmpc_time(k) = toc;
end

%% Test HTMPC-LDI
htmpc_ldi_time = [];

% Call once to avoid loading libraries during measured execution time
htmpc_ldi(zeros(size(system.a,2), 1));

for k = 1:n
    tic
    htmpc_ldi(xs(k, :)');
    htmpc_ldi_time(k) = toc;
end

%% Generate test report

disp('Timing results:')
disp('    OL-GCMPC:')
disp(['         Min   : ' num2str(1000 * min(ol_gcmpc_time)) ' ms'])
disp(['         Max   : ' num2str(1000 * max(ol_gcmpc_time)) ' ms'])
disp(['         Mean  : ' num2str(1000 * mean(ol_gcmpc_time)) ' ms'])
disp(['         Median: ' num2str(1000 * median(ol_gcmpc_time)) ' ms'])
disp(['         99%   : ' num2str(1000 * prctile(ol_gcmpc_time, 99)) ' ms'])
disp('    T-GCMPC:')
disp(['         Min   : ' num2str(1000 * min(t_gcmpc_time)) ' ms'])
disp(['         Max   : ' num2str(1000 * max(t_gcmpc_time)) ' ms'])
disp(['         Mean  : ' num2str(1000 * mean(t_gcmpc_time)) ' ms'])
disp(['         Median: ' num2str(1000 * median(t_gcmpc_time)) ' ms'])
disp(['         99%   : ' num2str(1000 * prctile(t_gcmpc_time, 99)) ' ms'])
disp('    HTMPC-LDI:')
disp(['         Min   : ' num2str(1000 * min(htmpc_ldi_time)) ' ms'])
disp(['         Max   : ' num2str(1000 * max(htmpc_ldi_time)) ' ms'])
disp(['         Mean  : ' num2str(1000 * mean(htmpc_ldi_time)) ' ms'])
disp(['         Median: ' num2str(1000 * median(htmpc_ldi_time)) ' ms'])
disp(['         99%   : ' num2str(1000 * prctile(htmpc_ldi_time, 99)) ' ms'])

%% Plot cost results
columnwidth = 8.63;
textwidth = 17.78;

fig = figure(1);
clf
set(gca, 'FontSize', 8)
hold on
boxplot(1000 * [ol_gcmpc_time', t_gcmpc_time', htmpc_ldi_time'], 'label', {'OL-GCMPC', 'T-GCMPC', 'HTMPC'}, 'whisker', 100)

grid on
ylabel('Mosek solve time [ms]')

fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 columnwidth 0.7 * columnwidth];
fig.PaperSize = [columnwidth 0.7 * columnwidth];

print -dpng -r300 time_comparison.png