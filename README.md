# Guaranteed Cost Approach to Robust Model Predictive Control of Uncertain Linear Systems

This repository implements the Tube Guaranteed Cost Model Predictive Controller (T-GCMPC) and the "Open Loop" Guaranteed Cost Model Predictive Controller (OL-GCMPC). It also contains complementary material for the paper "A Guaranteed Cost Approach to Robust Model Predictive Control of Uncertain Linear Systems", which defines the OL-GCMPC (or simply GCMPC, as named in the paper).

It contains:
- T-GCMPC and OL-GCMPC Matlab implementation;
- The source code for the numerical examples of the IEEE ACC 2017 paper;

## Pre-requisites

The source code in this repository requires:

- [MATLAB](http://www.mathworks.com/products/matlab/)

and

- [MATLAB Symbolic Toolbox](https://www.mathworks.com/products/symbolic.html) (for OL-GCMPC)
- [YALMIP Toolbox](http://users.isy.liu.se/johanl/yalmip/)
- [Mosek](https://www.mosek.com) (or other QP and SDP solvers)

Installed and referenced in MATLAB's path.

Note: Mosek has free-licenses for Academia.

### Installing pre-requisites

 1. Create a folder named **tbxmanager**
 2. Go to this folder in Matlab
 3. Execute `urlwrite('http://www.tbxmanager.com/tbxmanager.m', 'tbxmanager.m');`
 4. Execute `tbxmanager install yalmip`
 5. Edit/create startup.m in your Matlab startup folder and add `tbxmanager restorepath` there
 6. Install Mosek through the steps in their website/installer
 7. Add mosek to Matlab's path
 8. Add gcmpc folder to Matlab's path

### Running IEEE ACC 2017 examples

1. Open folder `ol_gcmpc_examples`
2. Create folder `results`
2. Execute `run.m`

Note: this examples contain the code as it was written for the paper, therefore it doesn't use the GCMPC module

## Citing

### Tube Guaranteed Cost Model Predictive Controller (T-GCMPC)

Currently in production.

### "Open Loop" Guaranteed Cost Model Predictive Controller (OL-GCMPC)

@inproceedings{massera2016guaranteed, author={C. M. Massera and M. H. Terra and D. F. Wolf}, booktitle={2017 American Control Conference (ACC)}, title={Guaranteed cost approach for robust model predictive control of uncertain linear systems}, year={2017}, pages={4135-4140}, doi={10.23919/ACC.2017.7963590}, month={May},}